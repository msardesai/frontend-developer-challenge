const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
	mode: "development",
	entry: {
		app: ["core-js/stable", "./src/index.js"]
	},
	output: {
		path: __dirname + "/dist/assets",
		filename: "bundle.js"
	},
	devtool: "source-map",
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader"
				}
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					// Creates `style` nodes from JS strings
					"style-loader",
					// Translates CSS into CommonJS
					"css-loader",
					// Compiles Sass to CSS
					"sass-loader",
				],
			},
			{
				test: /\.html$/,
				use: [
					{
						loader: "html-loader"
					}
				]
			}
		]
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: "./templates/index.html",
			filename: "./index.html"
		})
	]
}