const formWrapper = document.getElementById('formWrapper');
const formError = document.getElementById('formError');
const form = document.getElementById('subscribe');

const checkForm = () => {
	/* client side validation */
	// no input
	if ((form.firstName.value === '') || (form.lastName.value === '') || (form.email.value === '')) {
		formError.innerHTML = 'Fields cannot be blank';
		formError.style.display = 'block';

		// set focus
		if (form.firstName.value === '') {
			form.firstName.focus();
		} else if (form.lastName.value === '') {
			form.lastName.focus();
		} else if (form.email.value === '') {
			form.email.focus();
		}

		return false;
	}

	// invalid email
	if (!checkEmail(form.email.value)) {
		formError.innerHTML = 'Invalid email';
		formError.style.display = 'block';

		// focus on email field
		form.email.focus();

		return false;
	}

	// check that a radio button has been selected
	if (!checkRadioButtons(form.homeowner)) {
		formError.innerHTML = 'Please indicate if you are a homeowner';
		formError.style.display = 'block';

		return false;
	}

	// hide error box
	formError.style.display = 'none';

	// send request
	let formData = new FormData(document.forms.subscribe);
	const xhr = new XMLHttpRequest();
	xhr.open('POST', 'https://reqres.in/api/subscribe', true);
	xhr.onprogress = function (xhr) { console.log('bytes loaded:', xhr.loaded); };

	xhr.onerror = function () {
		// display error
		formError.innerHTML = 'An error has occurred. Please try again later.';
		formError.style.display = 'block';

		// hide the form
		form.style.display = 'none';
	};

	xhr.send(formData);

	xhr.onload = function () {
		// display success message
		formWrapper.innerHTML = '<p class="signup__success">Thank you for subscribing! You have been successfully added to our mailing list.</p>';
	}
}

// email validator
function checkEmail(email) {
	return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}

// check that a radio button has been selected
function checkRadioButtons(button) {
	let marker = -1;

	for (var i = button.length - 1; i > -1; i--) {
		if (button[i].checked) {
			marker = i;
			i = -1;
		}
	}

	if (marker <= -1)
		return false;

	return true;
}

export { checkForm };