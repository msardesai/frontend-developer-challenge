const apiKey = 'JQ1LE8WZCWJRAMXL';
let ul;

// get data for each symbol
const getAlphaVantageData = (symbols) => {
	// create ul
	ul = document.createElement('ul');
	ul.setAttribute('class', 'ticker__list');

	// get data for each symbol
	symbols.forEach((symbol) => {
		const url = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=' + symbol +
			'&outputsize=compact&datatype=json' +
			'&apikey=' + apiKey;

		requestFile(url, parseAlphaVantageData);
	})
}

// Alpha Vantage API request
function requestFile(url, callback) {
	const xhr = new XMLHttpRequest();
	xhr.callback = callback;
	xhr.open('GET', url, true);
	xhr.onload = function (xhr) { this.callback.apply(this, this.arguments); };
	xhr.onerror = function (xhr) { console.log('error:', xhr); };
	xhr.onprogress = function (xhr) { console.log('bytes loaded:', xhr.loaded); }; /// or something
	xhr.send(null);
}

// parse data returned from Alpha Vantage and append to DOM
function parseAlphaVantageData(xhr) {
	const response = this.responseText;

	if (response.slice(0, 1) !== '{') { return; } // not a json file

	const json = JSON.parse(response);

	// check the last 7 days to get the most recent date with data
	let today, yesterday;

	for(let i = 0; i < 7; i++) {
		today = new Date();
		today.setDate(today.getDate() - i);

		// format date for ticker calculations
		today = getProperDateFormat(today.toDateString());

		// when we get the day, exit loop
		if(json['Time Series (Daily)'][today] !== undefined) { break; }
	}

	// set yesterday
	yesterday = new Date(today);
	yesterday.setDate(yesterday.getDate());

	// format date for ticker calculations
	yesterday = getProperDateFormat(yesterday.toDateString());

	// get stock numbers to display
	const curr = json['Time Series (Daily)'][today];
	const prev = json['Time Series (Daily)'][yesterday];
	const symbol = json['Meta Data']['2. Symbol']

	const close = curr['4. close'];
	const prevClose = prev['4. close'];
	const change = close - prevClose;
	const percent = (change / prevClose) * 100;

	// append to dom
	appendToDom(symbol, close, change, percent);
}

// format dates to match json keys
const getProperDateFormat = (datestr) => {
	let formattedDate = new Date(Date.parse(datestr));
	let dd = String(formattedDate.getDate()).padStart(2, '0');
	let mm = String(formattedDate.getMonth() + 1).padStart(2, '0');
	let yyyy = formattedDate.getFullYear();

	formattedDate = yyyy + '-' + mm + '-' + dd;

	return (formattedDate);
}

const appendToDom = (symbol, close, change, percent) => {
	// create li
	const li = document.createElement('li');
	li.setAttribute('class', 'ticker__item');

	// check if change is positive or negative for arrow
	let direction;

	(change < 0) ? (direction = 'drop') : (direction = 'rise');

	// create spans for each datum
	const symbolHTML = '<span class="ticker__symbol">' + symbol + '</span>';
	const closeHTML = '<span class="ticker__close ticker__desktop">' + Number(close).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</span>';
	const changeHTML = '<span class="ticker__difference ticker__difference--' + direction + '">' + ((change > 0) ? "+" : "") + '<span class="ticker__desktop">' + change.toFixed(2) + ' (</span>' + Math.abs(percent).toFixed(2) + '%<span class="ticker__desktop">)</span> <img src="../images/stock-' + direction + '.svg" alt="' + direction + '"></span>';

	// append
	li.innerHTML = symbolHTML + closeHTML + changeHTML;
	ul.appendChild(li);
	document.getElementById('ticker').appendChild(ul);
}

export { getAlphaVantageData };