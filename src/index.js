import "core-js/stable";
import '../styles/index.scss';
import Vue from 'vue/dist/vue';

// import components/functions
import { getAlphaVantageData } from './components/ticker.js';
import { checkForm } from './components/form.js';

const symbols = ['MSFT', 'GOOGL', 'BTCUSD'];
let form, formError, firstName, lastName, email;

form = document.getElementById('subscribe');
formError = document.getElementById('formError');
firstName = document.getElementById('firstName');
lastName = document.getElementById('lastName');
email = document.getElementById('email');

document.addEventListener('DOMContentLoaded', (event) => {
	/* ticker */
	getAlphaVantageData(symbols);

	/* subscribe form validation and submit */
	form.addEventListener('submit', (e) => {
		e.preventDefault();

		// validate and submit form
		checkForm();
	});

	/* calculator */
	/*
		formula: M = P[i(1 + i)^n] / [i(1 + i)^n - 1]
		M = monthly mortgage payment
		P = principal (loan) amount = Home Price - Down Payment
		i = monthly interest rate = (annual interest rate % / 100 %) / 12 months
		n = number of payments over length of loan = (e.g.) 30 years * 12 months
	*/
	new Vue({
		el: '#calculator',
		data() {
			return {
				minprincipal: 0,
				price: 5000,
				downpayment: 1000,
				period: 30,
				interest: 3.25,
				total: 17.41,
				numpayments: 360,
				dasharray: '',
				dashoffset: ''
			}
		},
		created() {
			this.getChartPercentages(this.numpayments, this.total);
		},
		methods: {
			calculateMortgage() {
				// total payment calculation
				const principal = this.price - this.downpayment;
				const interest = (this.interest / 100) / 12;
				const numpayments = this.period * 12;
				const exp = Math.pow((1 + interest), numpayments);
				const total = ((principal * (interest * exp)) / (exp - 1));

				// radial chart
				this.getChartPercentages(numpayments, total);

				// format total
				this.total = total.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				this.numpayments = numpayments;
			},
			getChartPercentages(numpayments, total) {
				const circumference = 2 * Math.PI * 84;	// 84 = radius
				const noInt = ((this.price - this.downpayment) / numpayments);
				const noIntPercent = noInt / total;

				// get stroke dash values
				this.dasharray = circumference;
				this.dashoffset = circumference * (noIntPercent - 1);
			}
		},
		computed: {
			getDashArray() {
				return this.dasharray;
			},
			getDashOffset() {
				return this.dashoffset;
			}
		}
	});
});